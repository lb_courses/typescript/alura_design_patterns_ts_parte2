// pedidos
let pedido1 = new Pedido("Luan", 150);
let pedido2 = new Pedido("Camila", 250);
let filaDeTrabalho = new FilaDeTrabalho();
filaDeTrabalho.adiciona(new PagaPedido(pedido1));
filaDeTrabalho.adiciona(new PagaPedido(pedido2));
filaDeTrabalho.adiciona(new ConcluiPedido(pedido1));
filaDeTrabalho.processa();
