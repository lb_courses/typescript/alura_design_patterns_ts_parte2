class Soma {
    constructor(esquerda, direita) {
        this.esquerda = esquerda;
        this.direita = direita;
    }
    avalia() {
        let valorDaEsqueda = this.esquerda.avalia();
        let valorDaDireita = this.direita.avalia();
        return valorDaEsqueda + valorDaDireita;
    }
    getEsquerda() {
        return this.esquerda;
    }
    getDireita() {
        return this.direita;
    }
    // aula 5
    aceita(impressora) {
        impressora.visitaSoma(this);
    }
}
