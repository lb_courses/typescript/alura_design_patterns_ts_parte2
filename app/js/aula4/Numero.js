class Numero {
    constructor(numero) {
        this.numero = numero;
    }
    // aula 5
    avalia() {
        return this.numero;
    }
    getNumero() {
        return this.numero;
    }
    aceita(impressora) {
        impressora.sivitaNumero(this);
    }
}
