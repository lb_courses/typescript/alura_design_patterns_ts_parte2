class NotasMusicais {
    constructor() {
        this.notas = [];
        console.log("NotasMusicais");
        this.notas.push({ nome: "do", nota: new Do() });
        this.notas.push({ nome: "re", nota: new Re() });
        this.notas.push({ nome: "mi", nota: new Mi() });
        this.notas.push({ nome: "fa", nota: new Fa() });
        this.notas.push({ nome: "sol", nota: new Sol() });
        this.notas.push({ nome: "la", nota: new La() });
        this.notas.push({ nome: "si", nota: new Si() });
    }
    pega(nome) {
        return this.notas.find(nota => nota.nome === nome);
    }
}
