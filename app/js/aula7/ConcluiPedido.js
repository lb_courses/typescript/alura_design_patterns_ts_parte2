class ConcluiPedido {
    constructor(p) {
        this.p = p;
    }
    executa() {
        console.log("concluindo pedido do " + this.p.getCliente());
        this.p.finaliza();
    }
}
