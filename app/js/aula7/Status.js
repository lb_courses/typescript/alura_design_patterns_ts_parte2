var Status;
(function (Status) {
    Status[Status["NOVO"] = 0] = "NOVO";
    Status[Status["PROCESSANDO"] = 1] = "PROCESSANDO";
    Status[Status["PAGO"] = 2] = "PAGO";
    Status[Status["ITEM_SEPARADO"] = 3] = "ITEM_SEPARADO";
    Status[Status["ENTREGE"] = 4] = "ENTREGE";
})(Status || (Status = {}));
