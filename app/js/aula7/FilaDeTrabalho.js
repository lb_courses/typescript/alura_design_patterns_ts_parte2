class FilaDeTrabalho {
    constructor() {
        this.comandos = [];
    }
    adiciona(comando) {
        this.comandos.push(comando);
    }
    processa() {
        this.comandos.map(comando => {
            comando.executa();
        });
    }
}
