class Pedido {
    constructor(cliente, valor) {
        this.cliente = cliente;
        this.valor = valor;
        this.status = Status.NOVO;
    }
    paga() {
        this.status = Status.PAGO;
    }
    finaliza() {
        this.dataFinalizacao = new Date();
        this.status = Status.ENTREGE;
    }
    getCliente() {
        return this.cliente;
    }
}
