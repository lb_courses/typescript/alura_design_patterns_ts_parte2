class PagaPedido {
    constructor(p) {
        this.p = p;
    }
    executa() {
        console.log("pagando pedido do " + this.p.getCliente());
        this.p.paga();
    }
}
