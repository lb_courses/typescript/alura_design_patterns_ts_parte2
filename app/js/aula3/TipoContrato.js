var TipoContrato;
(function (TipoContrato) {
    TipoContrato[TipoContrato["NOVO"] = 0] = "NOVO";
    TipoContrato[TipoContrato["EM_ANDAMENTO"] = 1] = "EM_ANDAMENTO";
    TipoContrato[TipoContrato["ACERTADO"] = 2] = "ACERTADO";
    TipoContrato[TipoContrato["CONCLUIDO"] = 3] = "CONCLUIDO";
})(TipoContrato || (TipoContrato = {}));
