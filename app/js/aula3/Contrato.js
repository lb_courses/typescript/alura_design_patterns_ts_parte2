class Contrato {
    constructor(data, cliente, tipo) {
        // console.log("c -> ", tipo)
        this.data = data;
        this.cliente = cliente;
        this.tipo = tipo;
    }
    getData() {
        return this.data;
    }
    getClient() {
        return this.cliente;
    }
    getTipo() {
        return this.tipo;
    }
    avanca() {
        // fluxo dos status
        // console.log("tipo ->", this.tipo);
        if (this.tipo == TipoContrato.NOVO)
            this.tipo = TipoContrato.EM_ANDAMENTO;
        else if (this.tipo == TipoContrato.EM_ANDAMENTO)
            this.tipo = TipoContrato.ACERTADO;
        else if (this.tipo == TipoContrato.ACERTADO)
            this.tipo = TipoContrato.CONCLUIDO;
    }
    salvaEstado() {
        return new Estado(
        // passa o contrato atual
        new Contrato(this.data, this.cliente, this.tipo));
    }
}
