interface INota {
  nome:string,
  nota:Nota
}

class NotasMusicais {
  
  private notas:INota[] = [];

  constructor() {
    console.log("NotasMusicais")
    
    this.notas.push({ nome: "do" , nota: new Do() });
    this.notas.push( { nome:"re" , nota: new Re()} );
    this.notas.push( { nome:"mi" , nota: new Mi()} );
    this.notas.push( { nome:"fa" , nota: new Fa()} );
    this.notas.push( { nome:"sol", nota: new Sol()});
    this.notas.push( { nome:"la" , nota: new La()} );
    this.notas.push( { nome:"si" , nota: new Si()} );

  }


  public pega ( nome:string ){
        return this.notas.find( nota =>  nota.nome === nome );
  }
  
  
}