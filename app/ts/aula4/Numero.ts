class Numero implements Expressao{

  private numero:number;

  constructor(numero:number) {
   this.numero = numero; 
  }
  

  // aula 5
  avalia(){
    return this.numero;
  }

  getNumero(){
    return this.numero;
  }

  aceita(impressora:ImpressoraVisitor):void{
    impressora.sivitaNumero(this);
  }
}