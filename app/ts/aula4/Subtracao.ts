class Subtracao implements Expressao {
 
  private esquerda:Expressao;
  private direita:Expressao;

  constructor( esquerda :Expressao, direita:Expressao ) {
    this.esquerda = esquerda;
    this.direita = direita;
  }

  avalia(){
    let valorDaEsqueda = this.esquerda.avalia();
    let valorDaDireita = this.direita.avalia();
    return valorDaEsqueda - valorDaDireita;
  }

  public getEsquerda():Expressao{
    return this.esquerda;
  }
  public getDireita():Expressao{
    return this.direita;
  }
  
// aula 5
  aceita(impressora:ImpressoraVisitor):void{
    impressora.visitaSubtracao(this);
  }
  
}