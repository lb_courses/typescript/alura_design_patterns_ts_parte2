class Historico {

  private estadosSalvos: Estado[] = [];

  pega(index:number) {
    return this.estadosSalvos[index];
  }

  adicona(estado:Estado){
    this.estadosSalvos.push(estado);
  }

}