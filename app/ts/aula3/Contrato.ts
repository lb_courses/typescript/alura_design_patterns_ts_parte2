class Contrato {
  private data : Date;
  private cliente: string;
  private tipo: TipoContrato;


  constructor( data:Date, cliente:string, tipo:TipoContrato ){
      // console.log("c -> ", tipo)
    this.data = data;
    this.cliente = cliente;
    this.tipo =tipo;
  }

  getData(){
    return this.data;
  }

  getClient(){
    return this.cliente;
  }

  getTipo(){
    return this.tipo;
  }

  avanca() {
    // fluxo dos status
    // console.log("tipo ->", this.tipo);
    if( this.tipo == TipoContrato.NOVO) this.tipo = TipoContrato.EM_ANDAMENTO
    else if( this.tipo == TipoContrato.EM_ANDAMENTO) this.tipo = TipoContrato.ACERTADO
    else if( this.tipo == TipoContrato.ACERTADO) this.tipo = TipoContrato.CONCLUIDO
  }

  salvaEstado():Estado{
    return  new Estado(
      // passa o contrato atual
      new Contrato(this.data,this.cliente,this.tipo)
    )
  }
}