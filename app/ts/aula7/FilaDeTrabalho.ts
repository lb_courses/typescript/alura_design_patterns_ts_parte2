class FilaDeTrabalho {

  private comandos: Comando[];

  constructor(){
      this.comandos = [];
  }

  adiciona(comando  : Comando ){
  this.comandos.push(comando);
  }

  processa() {
    this.comandos.map( comando => {
      comando.executa();
    })
  }

}