class PagaPedido implements Comando {

  private p:Pedido;

  constructor(p:Pedido) {
    this.p = p;
  }

  executa(){
    console.log("pagando pedido do " + this.p.getCliente())
    this.p.paga();
  }

}