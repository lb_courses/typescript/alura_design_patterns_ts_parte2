class Pedido {
  private cliente:string;
  private valor: number;
  private status: Status;
  private dataFinalizacao: Date;

  constructor( cliente:string , valor:number) {
    this.cliente = cliente;
    this.valor = valor;
    this.status = Status.NOVO;
  }


  paga(){
    this.status = Status.PAGO;
  }

  finaliza(){
    this.dataFinalizacao = new Date();
    this.status = Status.ENTREGE;
  }

  getCliente(){
    return this.cliente;
  }

}