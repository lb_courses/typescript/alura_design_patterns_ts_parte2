class ConcluiPedido implements Comando{

  private p:Pedido;

  constructor(p:Pedido) {
    this.p = p;
  }

  executa(){
    console.log("concluindo pedido do " + this.p.getCliente())
    this.p.finaliza();
  }
}