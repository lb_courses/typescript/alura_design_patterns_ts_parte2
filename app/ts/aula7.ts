// pedidos

let pedido1:Pedido = new Pedido("Luan", 150);
let pedido2:Pedido = new Pedido("Camila", 250);

let filaDeTrabalho:FilaDeTrabalho = new FilaDeTrabalho();

filaDeTrabalho.adiciona(new PagaPedido(pedido1))
filaDeTrabalho.adiciona(new PagaPedido(pedido2))
filaDeTrabalho.adiciona(new ConcluiPedido(pedido1))

filaDeTrabalho.processa();